import React from 'react'
import api from '../../config/api'
import ReactPaginate from 'react-paginate'
import { createNotification } from '../../config/notification'
import Loader from '../../animation/Loader'

export default class Users extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      users: [],
      pageCount: 0,
      loading: false
    }
  }

  componentDidMount = () => this.getUserList(0)

  getUserList = page => {
    this.setState({ loading: true })
    api({
      url: `https://reqres.in/api/users?page=${page}`,
      method: 'get'
    })
      .then(res => {
        console.log(res.data)
        this.setState({ users: res.data.data, pageCount: res.data.total_pages })
        this.setState({ loading: false })
      })
      .catch(err => {
        console.log(err.message)
        createNotification('error', err.message)
        this.setState({ loading: false })
      })
  }

  renderUserCard = user => {
    return (
      <tr key={user.id}>
        <td>
          <img src={user.avatar} width={40} height={40} alt={'avatar'} />
        </td>
        <td> {user.first_name} </td>
        <td> {user.last_name} </td>
        <td>{user.email}</td>
      </tr>
    )
  }

  render () {
    return (
      <div className='container'>
        <h1>Users List</h1>
        {this.state.loading && <Loader height={20} width={150} animation='SimpleLoader' />}
        {!this.state.loading && (
          <table className='user-table'>
            <thead>
              <tr>
                <th>Avatar</th>
                <th>First_name</th>
                <th>Last_name</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {this.state.users.map(user => {
                return this.renderUserCard(user)
              })}
            </tbody>
          </table>
        )}
        <ReactPaginate
          pageCount={this.state.pageCount}
          pageRangeDisplayed={4}
          onPageChange={selectedItem => this.getUserList(selectedItem.selected)}
          containerClassName='pagination'
          pageClassName='page-item'
          activeClassName='active-page-item'
          activeLinkClassName='active-link-page-item'
          previousClassName='previous'
          nextClassName='next'
          previousLinkClassName='previous-link'
          nextLinkClassName='next-link'
          disabledClassName='disabled'
        />
      </div>
    )
  }
}
