# User Datatable

This is Test project to display user list.

Built with:
- React.JS
- webpack
- Babel
- ESLint
- and more

## installation

- clone the git repository
```
  npm install (or yarn)
```

## run
```
  npm start (or yarn start)
```

